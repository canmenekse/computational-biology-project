#!/usr/bin/perl
$|=1;
#Use
#use warnings;
# ---------------
# ENS210 � Computational Biology - Spring 2013
# Course Project � SNP Functionalization
# ---------------
# Group Members:
# Can Menekse
# Ozgun Osusta 
# Yavuz Selim Emir 
# ---------------
# Originality Statement:
# We only looked at some tutorial codes to how to use some modules, but even we modified them.
# Also used HTML::Strip module and HTML::TableExtract modules (they are not written by us ) 
# ---------------
use strict;
use	WWW::Mechanize;
use Data::Dumper;
use CGI;
use autodie qw/ open close /;
use HTML::TableExtract;
use HTML::Strip;
use Getopt::Long;
use 5.012;
my $printstd=*STDOUT; 
   
   
      
sub parseGenomeBrowser
{
my $link=shift(@_);
#print $link;
 my $mechVar=WWW::Mechanize->new(autocheck => 1);
$mechVar->get($link);
#$mechVar->get("http://genome.ucsc.edu/cgi-bin/hgc?hgsid=338030387&c=chr15&o=76669978&t=76669980&g=multiz44way&i=multiz44way");
     my $hs = HTML::Strip->new();
my $raw_html=$mechVar->content();
 my $nw_text = $hs->parse( $raw_html );
 $hs->eof;
 #print $OUT $nw_text;
 my $a_count=0;
 my $g=0;
 my $c=0;
 my $t=0;
 my $equal=0;
 my $minus=0;
 my $n=0;
 my $count=0;
 if($nw_text =~ m/bps\s+\n(.*?)View/gs)
 {
    
    my $tmp=$1;
    
    while($tmp=~m/([agctun\-=NAGCTU])\n/g)
    {
    
     my   $tempchar= $1;     
      $tempchar=~tr/AGCTN/agctn/;
     $count=$count+1;
      if($tempchar eq"a")
      {
        $a_count=$a_count+1;
      }
      elsif($tempchar eq "g")
      {
        $g=$g+1;
      }
      elsif($tempchar eq "c")
      {
        $c=$c+1;
      }
      elsif ($tempchar eq "n")
      {
        $n=$n+1;
      }
      elsif($tempchar eq "=")
      {
        $equal=$equal+1;
      }
      elsif($tempchar eq "-")
      {
        $minus=$minus+1;
      }
      elsif($tempchar eq "t")
      {
        $t=$t+1;
      }
    }
   
    
    
    my %hashA=("key"=>"A","value"=>$a);
    my %hashG=("key"=>"G","value"=>$g);
    my %hashC=("key"=>"C","value"=>$c);
    my %hashT=("key"=>"T","value"=>$t);
    my %hashN=("key"=>"N","value"=>$n);
    my %hashequal=("key"=>"=","value"=>$equal);
    my %hashminus=("key"=>"-","value"=>$minus);
    my @tempArrayOfHashes=(\%hashA,\%hashG,\%hashC,\%hashT,\%hashN,\%hashequal,\%hashminus);
    #print Dumper(@tempArrayOfHashes);
     my @sorted=sort { $a->{value} <=> $b->{value} } @tempArrayOfHashes;
     
    my $printtxt="";
    foreach my $hash(@sorted)
    {
       if($$hash{"value"}!=0)
       {
       my $temp=sprintf(" %s (%.3f\%)",$$hash{"key"},($$hash{"value"}*100)/$count);
       $printtxt=$printtxt.$temp;
       }
    }
    
    
    #my $printtxt=sprintf( "A (%.3f\%) G (%.3f\%) C (%.3f\%) T (%.3f\%) - (%.3f\%) = (%.3f\%)N (%.3f\%)",($a*100.0)/$count,($g*100.0)/$count,($c*100.0)/$count,($t*100.0)/$count,($minus*100.0)/$count
    
    #,($equal*100.0)/$count,($n*100.0)/$count);
    return $printtxt;




    } 
 }
   
   
   
   
   
   
   
   
   
   
   
   #Gets the snplist from .txt and put it in rsInfo
sub getSNPlist
{
        my $mechVar=shift(@_);
        my $rsInfo=shift(@_);
        my $pageLink=shift(@_);
        my $content=$mechVar->content();
        #get the snplist.txt
        my $txtlink=$content=~m/(snplist.*?\.txt)/;
        #matched value is txtlink
        $txtlink=$1;
        #replace http://snp-nexus.org/temp/snpnexus_3009/results.html with http://snp-nexus.org/temp/snpnexus_3009/snplist_3009.txt
        $pageLink=~s/results\.html/$txtlink/;
        #go to the url of the textfile
        $mechVar->get($pageLink);
        #get the txt file
        $content=$mechVar->content();
        #split the lines 
        my @lines = split /\n/, $content;
        #get the second line
        my $line=$lines[1];
        @$rsInfo = split(/\t/, $line);
        #split with tabs
   
 }
   
sub printContentsOfRsInfo
{
    my $rsInfo=shift(@_);
    my $snpID=@$rsInfo[0];
    my $chr=@$rsInfo[1];
    my $chrpos=@$rsInfo[2];
    my $Allele1=@$rsInfo[3];
    my $Allele2=@$rsInfo[4];
    my $contig=@$rsInfo[5];
    my $contigPos=@$rsInfo[6];
    my $bnd=@$rsInfo[7];
    my $dbSNP=@$rsInfo[8];
    my $hapMap=@$rsInfo[9];
    #print
    my $printstr=sprintf ("\nSNPID: %s \nchr: %s \nchrpos: %s \nAllele1: %s \nAllele2: %s \ncontig:%s \ncontigPos: %s \nbnd: \n%s \ndbSNP: %s \nhapmap: %s",$snpID,$chr,$chrpos,$Allele1,$Allele2,$contig,$contigPos,$bnd,$dbSNP,$hapMap);
    print $printstr;
 }
   
   
   
sub getSNPInfo
{
        my $mechVar=shift(@_);
        my $SNPInfoArrayOfHashes=shift(@_);
        my $pageLink=shift(@_);
        my $content=shift(@_);
        my $txtlink;
        #http://snp-nexus.org/temp/snpnexus_7561/refseq_7561.txt
       
       
        $content=~m/(snplist.*?\.txt)/;
        $txtlink=$1;
        #temp
        $pageLink=~s/results\.html/$txtlink/;
        #print"\n\n";
        $mechVar->get($pageLink);
        #print "link followed is " .$pageLink;
        $content=$mechVar->content();
        createArrayOfHash($content,$SNPInfoArrayOfHashes);
       # print "SNP INFO: ". "\n";
        #print Dumper($SNPInfoArrayOfHashes);
} 
   
   
sub getRefSeq
{
        ##Sparse matrice
        my $mechVar=shift(@_);
        my $RefSeqArrayOfHashes=shift(@_);
        my $pageLink=shift(@_);
        my $content=$mechVar->content();
        my $txtlink;
        #http://snp-nexus.org/temp/snpnexus_7561/refseq_7561.txt
       
        $content=~m/(refseq.*?\.txt)/;
        #$content=~m/(tfbs.*?\.txt)/;
        $txtlink=$1;
        #temp
        $pageLink=~s/results\.html/$txtlink/;
       # print"\n\n";
        $mechVar->get($pageLink);
       # print "link followed is " .$pageLink;
        $content=$mechVar->content();
        createArrayOfHash($content,$RefSeqArrayOfHashes);
        #print Dumper($RefSeqArrayOfHashes);
       
 }
 
 sub getTranscriptionFactorBindingSites
 {
     ##Sparse matrice
        my $mechVar=shift(@_);
        my $TFBSArrayofHashes=shift(@_);
        my $pageLink=shift(@_);
        my $content=shift(@_);
        my $txtlink;
        #http://snp-nexus.org/temp/snpnexus_7561/refseq_7561.txt
       
       
        $content=~m/(tfbs.*?\.txt)/;
        $txtlink=$1;
        #temp
        $pageLink=~s/results\.html/$txtlink/;
        #print"\n\n";
        $mechVar->get($pageLink);
        #print "link followed is " .$pageLink;
        $content=$mechVar->content();
        createArrayOfHash($content,$TFBSArrayofHashes);
        #print "TFBS ". "\n";
       # print Dumper($TFBSArrayofHashes);
 
 
 }
 
 
  sub getMIRNAs
 {
     ##Sparse matrice
        my $mechVar=shift(@_);
        my $MIRNAArrayOfHashes=shift(@_);
        my $pageLink=shift(@_);
        my $content=shift(@_);
        my $txtlink;
        #http://snp-nexus.org/temp/snpnexus_7561/refseq_7561.txt
       
       
        $content=~m/(mirbase.*?\.txt)/;
        $txtlink=$1;
        #temp
        $pageLink=~s/results\.html/$txtlink/;
        #print"\n\n";
        $mechVar->get($pageLink);
        #print "link followed is " .$pageLink;
        $content=$mechVar->content();
        createArrayOfHash($content,$MIRNAArrayOfHashes);
        #print "MIRNA: ". "\n";
        #print Dumper($MIRNAArrayOfHashes);
 
 
 }
 
 
 
 
 
 sub createArrayOfHash
 {
    
    my $content=shift(@_);
    #print $content;
    my $arrayOfHash=shift(@_);
    my @lines = split /\n/, $content;
    my $line=$lines[0];
    if(exists($lines[0]))
    {
        
        my %columnNames;
       my @column = split(/\t+/, $line);
       
          for(my $i=0;$i<(@column);$i=$i+1)
            {
                $columnNames{$i}=$column[$i];
            }
        
        for(my $lineIndex=1;$lineIndex<@lines;$lineIndex=$lineIndex+1)
            {
                 my %rowHash;
                @column = split(/\t/, $lines[$lineIndex]);
                for(my $wordIndex=0;$wordIndex<@column;$wordIndex=$wordIndex+1)
                {
                   
                    my $hashIndex=$columnNames{$wordIndex};
                    $rowHash{$hashIndex}=$column[$wordIndex];
                    
                }
               push(@$arrayOfHash,\%rowHash);
            }
     }
     else
     {
        #print " file is empty";
     }
    
 }
       
       
        
   
   
sub printHash
{
        #print "\n\n A hash:\n\n" ;
        my $hshRef=shift(@_);
        my %hsh=%$hshRef;
         foreach my $key (keys %hsh)
        {
        #print $key. " =>". $hsh{$key}."\n";
        }
   }
   sub printArrayOfHash
   {
        my $ArrayOfHash=shift(@_);
        
}
    
    
        
    
   
   
   
   
   
   
   
   #Puts the content in rsInfo
sub parseSNPNexus
{
    ##rs41279484
    my $mechVar=shift(@_);
    my $RefSeqArrayOfHashes=shift(@_);#push with @$
    my $TFBSArrayofHashes=shift(@_);
    my $MIRNAArrayOfHashes=shift(@_);
    my $SNPInfoArrayOfHashes=shift(@_);
    my $ucscLinkRequirement=shift(@_);
    my $snpID=shift(@_);
    
    my $url="http://snp-nexus.org/cgi-bin/snp/s5_48.cgi";
    ##Post the form
    $mechVar->post($url,[
    'refseq'=>"refseq",
    'mirbase'=>"mirbase",
    'dbsnp_id'=>$snpID, ##'dbsnp_id'=>"rs12975333",
    'tfbs'=>"tfbs",
    'assembly'=>"hg18",
    'query'=>"dbsnp",
    'Type'=>"Chromosome",
    'vcf'=>"txt",
    'chrom'=>1,
    'stand'=>1
    ]);
    
    my $content;
    
    my $mechanizeLink= $mechVar->find_link(text_regex=>qr/\.html/i);
    sleep 5;
    my $pageLink=$mechanizeLink->url;
    #we will need that link later
    my $pageLinkDup=$pageLink;
    #go to the snp nexus info page
    $mechVar->get($mechanizeLink->url);
    ##get the uscs link
   # print $mechVar->content();
   if($mechVar->content=~m/high/i)
   {
        print "SNP nexus has issues ";
   }
    
    
    #print" PRINTING URL : " . $ucscLinkUrl;
    #get the content to search for .txt file
    my $contentOfTheDomain=$mechVar->content();
    $contentOfTheDomain=~/(http\:\/\/genome.ucsc.*?\>)/i;
    my $ucscLink=$1;
    
   # print "THE LINK IS ". $ucscLink;
    $ucscLink=~/(\d+)\-(\d+)/;
    $$ucscLinkRequirement=$1;
    #getSNPlist($mechVar,$rsInfo,$pageLink);
    $pageLink=$pageLinkDup;
    #Prints the assigned value 
   # printContentsOfRsInfo($rsInfo);
    $mechVar->get($pageLink);
    ##gets the refseq
    getRefSeq($mechVar,$RefSeqArrayOfHashes,$pageLink);
    getTranscriptionFactorBindingSites($mechVar,$TFBSArrayofHashes,$pageLink,$contentOfTheDomain);
    getMIRNAs($mechVar,$MIRNAArrayOfHashes,$pageLink,$contentOfTheDomain);
    getSNPInfo($mechVar,$SNPInfoArrayOfHashes,$pageLink,$contentOfTheDomain);
    
    
    
    
   
}


sub addWhenKeyExists
{
    my $hashRef=shift(@_);
    my $singlePrintHashRef=shift(@_);
    my $mappingsRef=shift(@_);
   my %mappings=%$mappingsRef;
   my %currentHash=%$hashRef;
   
    foreach my $key (keys %currentHash)
    {
        if(exists $mappings{$key})
        {
            
            $$singlePrintHashRef{$mappings{$key}}= $currentHash{$key};
        }
    }
        
}









sub MapNcbi
{
my $NcbiRefSnpRef=shift(@_);
my $AlleleRefSnpRef=shift(@_);
my $hgvsArrayRef= shift(@_);
my $singlePrintHashRef=shift(@_);      
      my %mappings=("Variation Class"=>"VAR_CLASS","Organism"=>"ORGANISM");
      addWhenKeyExists($NcbiRefSnpRef,$singlePrintHashRef,\%mappings);
      addWhenKeyExists($AlleleRefSnpRef,$singlePrintHashRef,\%mappings);
     # print Dumper($singlePrintHashRef);

}




sub parseNcbi
{
   
    my $mechVar=shift(@_);
    my $NcbiRefSnp=shift(@_);
    my $AlleleRefSnp=shift(@_);
    my $hgvsArrayRef=shift(@_);
    ##my $snpId="rs12975333";
    my $snpId =shift(@_);
    my $pageLink=sprintf("http://www.ncbi.nlm.nih.gov/projects/SNP/snp_ref.cgi?searchType=adhoc_search&type=rs&rs=%s",$snpId);
    #print $pageLink;
    $mechVar->get($pageLink);
    my $content=$mechVar->content;
    #Parse Refsnp
    parseNcbiRefSNPandAllele($content,$NcbiRefSnp,"RefSNP");
    #Parse Allele
    parseNcbiRefSNPandAllele($content,$AlleleRefSnp,"Allele");
    parseHgvsNames($content,$hgvsArrayRef);
    #print Dumper($NcbiRefSnp);
    #print Dumper($AlleleRefSnp);
    #print Dumper($hgvsArrayRef);
 }
 
    
sub parseHgvsNames
{
    
    
    my $content=shift(@_);
    my $hgvsArrayRef=shift(@_);
    
    my $te = HTML::TableExtract->new();;
    $content=~m/(<TABLE\s+ id="HGVS Names".*?<\/TABLE>)/m;
    my $RefSNPTable=$1;
    
    $te->parse($RefSNPTable);
 # Examine all matching tables
    foreach my $ts ($te->tables) 
    {
   
        foreach my $row ($ts->rows) {
        my $matches= join (',,', @$row), "\n";
        
        $matches =~ s/[\x0A\x0D]//g;
        if(!($matches=~m/HGVS/))
        {
        push(@$hgvsArrayRef,$matches);
        }
      
           
            }
        }
 }

    

    
    





sub parseNcbiRefSNPandAllele
{
    
    my $content=shift(@_);
    my $hashRef=shift(@_);
    my $id=shift(@_);
    my $te = HTML::TableExtract->new();;
    #Find the tble to be parsed
    $content=~m/(<TABLE\s+ id="$id".*?<\/TABLE>)/m;
    #Get it
    my $RefSNPTable=$1;
   
    #parse
    $te->parse($RefSNPTable);
 # Examine all matching tables
    #Actually there is only one
    foreach my $ts ($te->tables) 
    {
  
        foreach my $row ($ts->rows) {
        my $matches= join (',,', @$row), "\n";
        
        #remove non ascii
        $matches =~ s/[\x0A\x0D]//g;
        $matches=$matches."\n";
            if($matches=~m/(.*?):,,(.*)/)
            {
            #print " it is " . $1 ." and ". $2 ;
            $$hashRef{$1}=$2;
           
            }
        }
    }
}








sub printSnpNexusHashes
{
    my $hashRef=shift(@_);
    my $printedHashName=shift(@_);
    #check again
    my $mappingsRef=shift(@_);
    my $layoutText=shift(@_);
    my %mappings=%$mappingsRef;
    
    my %currentHash=%$hashRef;
    print $layoutText "\n".$printedHashName."\n";
    foreach my $key (keys %currentHash)
    {
           if(exists $mappings{$key})
           {
                if($currentHash{$key} ne "")
                {
                print $layoutText "\t". $mappings{$key}. " = ". $currentHash{$key} ."\n" ;
                }
                
           }
    
    
    }
   #print $printedHashName."\n";
}

sub printSnpNexus
{
    my $arrayRef=shift(@_);
    my $mappings=shift(@_);
    my $arrayName=shift(@_);
    my $layoutText=shift(@_);
    my @tempArray=@$arrayRef;
    
    for( my $i=0;$i<(@tempArray);$i=$i+1)
    {
            if (ref $tempArray[$i])
        {
       # print "This is a reference\n";
        }
        else
        {
        #print "This is a normal variable\n";
        }
            
            printSnpNexusHashes($tempArray[$i],$arrayName,$mappings,$layoutText);
    }
}



#What it does is that it searches the tables in the page and 
sub parseFsnp
{
    
    my $mechVar=shift(@_);
    my $fsnpHash=shift(@_);
    my $snpId=shift(@_);
    my $singlePrintHashRef=shift(@_);
    my $MetaArray=shift(@_);
    my $pageLink=sprintf("http://compbio.cs.queensu.ca/cgi-bin/compbio/search/main.cgi?search_mode=id&id_type=snp_id&id_val=%s&disease_category=dl0&disease_name=0&gene_name=&chr=1&start_pos=&end_pos=",$snpId);
   
    $mechVar->get($pageLink);
    my $content=$mechVar->content;
    #print $OUT $content;
    #Print all tables
     my $te = HTML::TableExtract->new();
     $te->parse($content);
   
    
    
    #
    
    if($content=~m/<table cellSpacing="0.5em" cellPadding="0.5em" border="0">/)
    {
      # print "MATCH";
       my $te = HTML::TableExtract->new();
       $te->parse($content);
       my $ts=$te->table(1,0);
       #print $OUT "Table (", join(',', $ts->coords),"):\n";
       foreach my $row ($ts->rows) {
       my $matches= join (',', @$row), "\n";
       $matches =~ s/[\x0A\x0D]//g;
       $matches=~m/(\w+):,\s,(.*)/;
       $$fsnpHash{$1}=$2;
       #print $OUT $matches ."\n";
       }
       #print Dumper($fsnpHash);
        ###############
        my $snpFs;
         $ts=$te->table(0,2);
       # print $OUT "Table (", join(',', $ts->coords),"):\n";
        foreach my $row ($ts->rows) {
        my $matches= join ('aeawe,heyhawyeyhew', @$row), "\n";
        $matches =~ s/[\x0A\x0D]//g;
        $matches=~m/FS\s+score\s+([0-9\.]+)/;
        $snpFs=$1;
        }
        
        #print "snpFs is ". $snpFs;
        $$singlePrintHashRef{"FS_SCORE"}=$snpFs;
        ##################################################BURAYA BAK!!!!
        $ts=$te->table(1,1);
        foreach my $row ($ts->rows) {
        #my $matches= join ('\n', @$row), "\a";
            foreach my $tempvalue (@$row)
            {
                #print $tempvalue."\n";
                push(@$MetaArray,$tempvalue);
            }
        #$matches =~ s/[\x0A\x0D]//g;
        #push(@tempArray,$matches);
        }
       # print Dumper(@$MetaArray);
        my $size = @$MetaArray;
		my $prot = -1;
		my $splice = -1;
		my $conserved = -1;
		for(my $i=0;$i<$size;$i++)
		{
			if(index(@$MetaArray[$i],"protein_coding") != -1)
			{
				$prot = $i;
			}
			elsif(index(@$MetaArray[$i],"splicing_regulation") != -1)
			{
				$splice = $i;
			}
			elsif(index(@$MetaArray[$i],"conserved") != -1 && $conserved == -1)
			{
				$conserved = $i;
			}
		}
		if($prot != -1)
		{
			my @predictions;
			push(@predictions, @$MetaArray[$prot + 2]);
			push(@predictions, @$MetaArray[$prot + 6]);
			push(@predictions, @$MetaArray[$prot + 10]);
			push(@predictions, @$MetaArray[$prot + 14]);
			push(@predictions, @$MetaArray[$prot + 18]);
			push(@predictions, @$MetaArray[$prot + 22]);
			my $maxpos = 0;
			my $curpos = 0;
			my $max = 0;
			my $count = 0;
			foreach my $prediction ( @predictions)
			{
				$count=0;
				foreach my $pred2 ( @predictions)
				{
					if($prediction eq $pred2)
					{
						$count++;
					}
				}
				if($count > $max)
				{
					$maxpos = $curpos;
					$max = $count;
				}
				$curpos++;
			}
			$$singlePrintHashRef{"protein_coding"} = $predictions[$maxpos]. " " . "$max" . "/6" ." (".($max/6.0)*100 ."\%)";
		}
 		if($splice != -1)
		{
			my @predictions;
			push(@predictions, @$MetaArray[$splice + 2]);
			push(@predictions, @$MetaArray[$splice + 6]);
			push(@predictions, @$MetaArray[$splice + 10]);
			push(@predictions, @$MetaArray[$splice + 14]);
		
			my $maxpos = 0;
			my $curpos = 0;
			my $max = 0;
			my $count = 0;
			foreach my $prediction ( @predictions)
			{
				$count=0;
				foreach my $pred2 ( @predictions)
				{
					if($prediction eq $pred2)
					{
						$count++;
					}
				}
				if($count > $max)
				{
					$maxpos = $curpos;
					$max = $count;
				}
				$curpos++;
			}
			$$singlePrintHashRef{"splicing_regulation"} = $predictions[$maxpos]. " " . "$max" . "/4" ." (".($max/4.0)*100 ."\%)";
		}       
        if($conserved != -1)
		{
			my $total = 1;
			if(@$MetaArray[$conserved + 2] eq @$MetaArray[$conserved + 6])
			{
				$total = 2;
			}
			$$singlePrintHashRef{"conserved"} = @$MetaArray[$conserved + 2]. " " . "$total" . "/2" ." (".($total/2.0)*100 ."\%)";
		}
        
    }


}


sub showEmptyInSinglePrint
{
    my $singlePrintHashRef=shift(@_);
    foreach my $key (%$singlePrintHashRef)
    {
       
        if($$singlePrintHashRef{$key} eq "" )
        {
            $$singlePrintHashRef{$key}="N/A";
        }
    }
}

sub mapSNPInfo
{
    my $hashRef=shift(@_);
    my $mappingsRef=shift(@_);
    my $singlePrintHashRef=shift(@_);
    my %mappings=%$mappingsRef;
    
    my %currentHash=%$hashRef;
 
    foreach my $key (keys %currentHash)
    {
           if(exists $mappings{$key})
           {
            $$singlePrintHashRef{$mappings{$key}}=$currentHash{$key} ;
           }
    
    
    }
   
}



sub createUcscLink
{
        my $Pos=shift(@_);
        my $charPos=shift(@_);
        my $link=sprintf('http://genome.ucsc.edu/cgi-bin/hgc?hgsid=338043065&c=%s&o=%d&g=multiz44way&i=multiz44way',$charPos,$Pos);
        return $link;
        
}
sub addRsInfoToSinglePrint
{
    #my $singlePrintRef=shift(@_);
    #my $arrayRef=shift(@_);
   # my @rsArray=@$arrayRef;
    #$$hashRef{"SNP_ID"}=$rsArray[0];
    #$hashRef{"LOCATION"}=$rsArray[1].",".$rsArray[2];
    #$hashRef{"ALLELES"}=$rsArray[3]."|"$rsArray[4];
    #$hashRef{"NUCLEOTIDE_CHANGE"}=$rsArray[3].">"$rsArray[4];
}

#sub parseMetaArray
#{
   #my  $MetaArrayRef=shift(@_);
    
    #if(checkValidMetaArray($MetaArrayRef)==1)
    #{
        # return "N/AN/AN/AN/AN/AN/AN/AN/AN/AN/AN/AN/AN/AN/A";
        
    #}
    #return "heyyyyyyyyyyyyyyyyyy";


#}
sub checkValidMetaArray
{
    my $MetaArrayRef=shift(@_);
    my @MetaArray=@$MetaArrayRef;
    foreach my $value (@MetaArray)
        {
            if($value=~ "protein_coding"||$value=~ "splicing_regulation"
            ||$value=~"post_translation")
            {
                return 1;
            }
            #print " check != " .$value . "\n";
        }
    return 0;
}   
   
   
sub mergeAlleles
{
    my $singlePrintHashRef=shift(@_);
    my %hash=%$singlePrintHashRef;
    if($hash{"ALLELE1"}eq "-"||$hash{"ALLELE2"} eq  "-")
    {
        return "N/A";
    }
    return $hash{"ALLELE1"}.">".$hash{"ALLELE2"};
   
}
sub printWhenKeyExists
{
    my $singlePrintHashRef=shift(@_);
    my $key=shift(@_);
    my %hash=%$singlePrintHashRef;
   
    if((exists $hash{$key}))
    {
       return $hash{$key};
    }
    return "N/A";
}
  
my $input_file="";

my $input_snp = "";
my $output_file = "";
my $help;
GetOptions("list=s" => \$input_file,
"snp=s" => \$input_snp,
"out=s" => \$output_file,
"help" => \$help,
);

if(!$input_file && !$input_snp && !$help)
{
    print "You should provide at least one SNP.\n";
    $help = 1;
}
elsif($input_file && $input_snp && !$help)
{
print "You should provide either a single SNP or a SNP";
$help = 1;
}



if($help)
{
    print "HELP:" ."\n"."-list: a txt file contains an id per line "."\n".
    "-snp: a snp ID "."\n"."-out: Where to output(When you dont specify ,prints to command l."."\n";
    exit 0;
}
    my @inputSNPs;
    
    my $inputFilename=$input_file;
    my $singleSNP=$input_snp;
    my $layoutText; 
    if($inputFilename ne"")
    {
        # print "opened file";
        open my $IN ,"<$inputFilename" or die "Unable to open $ARGV[0]\n" ;
        @inputSNPs=<$IN>;
    }
    elsif ($singleSNP ne "")
    {
        push(@inputSNPs,$singleSNP);
    }
     print "We are now computing your results,Please Wait!This may take a while"."\n"."If you do not get the result that probably means SNP nexus is down do not panic they tend to crash a lot,when that is the case  try again later"."\n";
     print "\n";
    #open (my $OUT, '>output7.html');
    #open (my $OUT2, '>output2.html');
    $layoutText=*STDOUT;
    #print $output_file;
     if($output_file ne "")
    {
       open ($layoutText, ">$output_file");
       open ($layoutText, ">>$output_file");
       
     
    }
 ##############################################

foreach my $snpIdValue(@inputSNPs)
 {
   print $layoutText "\n";
    #print "Please enter the ID of the SNP you want to enter\n";
    #my $snpID="rs252";
    #my $snpID="rs16969968";
    my $snpID=$snpIdValue;
    #Mechanize
    my @RefSeqArrayOfHashes;
    my @TFBSArrayOfHashes;
    my @MIRNAArrayOfHashes;
    my @SNPInfoArrayOfHashes;
    my @MetaArray;
    #Delete it
    my $ucscLinkRequirement;
    #
    my $uscsPos;
 
 
 
 
 
 
 

my %NcbiRefSnp;
my %AlleleRefSnp;
my @hgvsArray;
my %fsnpHash;
my %mappedNcbi;
my %singlePrintHash;
my %RefSeqMappings=("Gene"=>"REFSEQ_GENE","Strand"=>"REFSEQ_STRAND","Transcript"=>"REFSEQ_TRANSCRIPT",
"Predicted function"=>"REFSEQ_FUNCTION","cdna_pos"=>"REFSEQ_CDNA_POS","cds_pos"=>"REFSEQ_CDS_POS","splice_dist"=>"REFSEQ_SPLICE_DIST","Detail"=>"REFSEQ_MUTATION_TYPE"
,"aa_pos"=>"REFSEQ_AA_POS","aa_change"=>"REFSEQ_AA_CHANGE");
my %TFBSMappings=("TFBS_Accession"=>"TFBS_ACCESSION","TFBS_name"=>"TFBS_NAME","TFBS_Species"=>"TFBS_SPECIES");
my %MIRNAMappings=("Name"=>"MIRNA_NAME","Accession"=>"MIRNA_ACCESSION","Strand"=>"MIRNA_STRAND","Type"=>"MIRNA_TYPE");
my %SNPInfoMappings=("SNP"=>"SNP_ID","Chromosome"=>"CHROMOSOME","chromPosition"=>"CHROMPOSITION","Allele1"=>"ALLELE1",
"Allele2"=>"ALLELE2","Contig"=>"CONTIG","contigPosition"=>"CONTIGPOSITION","Band"=>"BAND","dbSNP"=>"DBSNP","HapMap"=>"HMAP");



my $mechVar	=WWW::Mechanize->new(autocheck => 1);
#Parses SNP Nexus
parseSNPNexus($mechVar,\@RefSeqArrayOfHashes,\@TFBSArrayOfHashes,\@MIRNAArrayOfHashes,\@SNPInfoArrayOfHashes,\$ucscLinkRequirement,$snpID);
mapSNPInfo($SNPInfoArrayOfHashes[0],\%SNPInfoMappings,\%singlePrintHash);
my $ucscPos=$singlePrintHash{"CHROMPOSITION"}-1;
my $ucscCharP=$singlePrintHash{"CHROMOSOME"};
#print "UCSC Link Requirement is " . $ucscPos ." and  " . $ucscCharP ;
my $ucscLink=createUcscLink($ucscPos,$ucscCharP);
#print  "ucscLink is :" .$ucscLink."\n\n\n\n";
#$mechVar->get($ucscLink);



#Parses Fsnp
parseFsnp($mechVar,\%fsnpHash,$snpID,\%singlePrintHash,\@MetaArray);
parseNcbi($mechVar,\%NcbiRefSnp,\%AlleleRefSnp,\@hgvsArray,$snpID);
MapNcbi(\%NcbiRefSnp,\%AlleleRefSnp,\@hgvsArray,\%singlePrintHash);
showEmptyInSinglePrint(\%singlePrintHash);
$singlePrintHash{'CONSERVATION'}=parseGenomeBrowser($ucscLink);
#print parseGenomeBrowser($ucscLink); 
#my $printTxt=sprintf("SNP_ID=%s\nLOCATION=%s\nALLELES=%s\nNUCLEOTIDE_CHANGE=%s\nVAR_CLASS=%s\nORGANISM=%s\nFS_SCORE=%s\nPROTEIN_CODING=%s\nSPLICING_REGULATION=%s\nCONSERVED=%s\nCONSERVATION_44=%s",
#$singlePrintHash{"SNP_ID"},$singlePrintHash{"CHROMOSOME"}.",".$singlePrintHash{"CHROMPOSITION"},
#$singlePrintHash{"ALLELE1"}."|".$singlePrintHash{"ALLELE2"},mergeAlleles(\%singlePrintHash),
#$singlePrintHash{"VAR_CLASS"},$singlePrintHash{"ORGANISM"},$singlePrintHash{"FS_SCORE"},printWhenKeyExists(\%singlePrintHash,"protein_coding"),printWhenKeyExists(\%singlePrintHash,"splicing_regulation")
#,printWhenKeyExists(\%singlePrintHash,"conserved"),$singlePrintHash{'CONSERVATION'});
my $printTxt=sprintf("SNP_ID=%s\nVAR_CLASS=%s\nORGANISM=%s\nLOCATION=%s\nALLELES=%s\nNUCLEOTIDE_CHANGE=%s\nCONSERVATION_44=%s\nMETA_CONSERVATION=%s\nFS_SCORE=%s\nMETA_PROTEIN_CODING=%s\nMETA_SPLICING_SIGNAL=%s",
$singlePrintHash{"SNP_ID"},$singlePrintHash{"VAR_CLASS"},$singlePrintHash{"ORGANISM"},$singlePrintHash{"CHROMOSOME"}.
",".$singlePrintHash{"CHROMPOSITION"},$singlePrintHash{"ALLELE1"}."|".$singlePrintHash{"ALLELE2"},mergeAlleles(\%singlePrintHash),
$singlePrintHash{'CONSERVATION'},printWhenKeyExists(\%singlePrintHash,"conserved"),$singlePrintHash{"FS_SCORE"},printWhenKeyExists(\%singlePrintHash,"protein_coding"),
printWhenKeyExists(\%singlePrintHash,"splicing_regulation"));



#print $layoutText "SNP_ID= ". $singlePrintHash{"SNP_ID"} ."\n"."LOCATION= " .$singlePrintHash{"LOCATION"} ."\n".
#"ALLELES= " .$singlePrintHash{"ALLELES"} ."\n"."ALLELES= " .$singlePrintHash{"NUCLEOTIDE_CHANGE"}. "\n" .
#"VAR_CLASS: ". $singlePrintHash{"VAR_CLASS"} . "\n" ."ORGANISM: " . $singlePrintHash{"ORGANISM"}."\n";
print $layoutText $printTxt;
printSnpNexus(\@RefSeqArrayOfHashes,\%RefSeqMappings,"REFSEQ_ENTRY",$layoutText);
printSnpNexus(\@TFBSArrayOfHashes,\%TFBSMappings,"TFBS_ENTRY",$layoutText);
printSnpNexus(\@MIRNAArrayOfHashes,\%MIRNAMappings,"MIRNA_ENTRY",$layoutText);
# parseMetaArray(\@MetaArray);
print $layoutText "//";
 }
